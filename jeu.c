#include <ncurses.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#include "_n_curses.h"

#define _DEBUG
#define TEXT 1
#define NB_BRICK 21
#define VITESSE_PLAYER 3

void f_jeu() {
	srand(time(NULL));
	ncurses_initialiser();

	unsigned int diff = 0;
	int height =2, width = 20, lstarty = (LINES - height), lstartx = COLS/2 - width/2; // dimension de la fenêtre joueur

	WINDOW *barre_win = create_newwin_standard(5,COLS,0,0); // création de la fenêtre barre en haut du jeu 
	WIN barre;
	barre.win=barre_win;
	barre.height = 5;
	barre.width = COLS;
	barre.posX = 0;
	barre.posY = 0;
	barre.score = 0;

	// Initialisation du joueur
	WIN player;
	init_player(&player, height, width, lstarty, lstartx);
	
	// Initialisation puis création de la brique
	BRICKS_BAND bricks_band;
	init_bricks_band(&bricks_band, &barre);
	
	// Initialisation de la balle
	BALL ball;
	init_ball(&ball);
	
	// Init couleur et Vérifie si le terminal supporte les couleurs
	ncurses_couleurs(); 

	// Initalisation du temps
	struct timeval temps_i; // tv_sec = seconds, tv_usec = nanoseconds
	struct timeval temps_f;
	gettimeofday(&temps_i, NULL);

	int ch = 0;
	bool done = false;
	while(!done) {
		// Process player input
		ch = getch(); //récupération des touches appuyées
		switch (ch) {
			case KEY_LEFT: // touche gauche alors on déplace le joueur à gauche
				destroy_win(player.win);
				if(player.posX > 0)
	  			{
      				player.posX=player.posX-VITESSE_PLAYER;
	  			}
				player.win = create_newwin_standard(height, width, player.posY, player.posX);
				break;
			case KEY_RIGHT: // touche droite alors on déplace le joueur à droite
				destroy_win(player.win);
				if(player.posX + player.width + 1 <= COLS)
	  			{
      				player.posX=player.posX+VITESSE_PLAYER;
	  			}
				player.win = create_newwin_standard(height, width, player.posY, player.posX);
				break;
			case 'r': // Reset la position de la balle et du joueur
				destroy_win(player.win);
				player.posX = lstartx;
				player.posY = lstarty;
				player.win = create_newwin_standard(height, width, player.posY, player.posX);
				reset_ball(&ball);
				break;
			case 'q': // quitter le jeu en fonctionnement
				done = true;
				destroy_win(ball.win.win);
				destroy_win(player.win);
				destroy_win(barre.win);
				for(int i = 0; i < NB_BRICK; i++)
				{
				if(bricks_band.brick[i].brick_break == false){ // destruction des briques pas encore détruitent
				destroy_win(bricks_band.brick[i].win.win);
				}
				}
				break;
		}
		doupdate(); //permet de refresh après un wnoutrefresh(), cela permet de ne pas refresh trop de fois ce qui fait clignoter le jeu
		
		// Ce qui suit permet de faire varier la vitesse de la balle et gére les collisions avec les obstacles
		gettimeofday(&temps_f, NULL); //récupération du temps final
		diff = ((temps_f.tv_sec * 1000) + (temps_f.tv_usec / 1000))
			- ((temps_i.tv_sec * 1000) + (temps_i.tv_usec / 1000));
		if (diff >= ball.vitesse) {
			inc_ball(&ball);
			for(int i = 0; i < NB_BRICK; i++)
			{
			bounce_ball(&ball, &bricks_band.brick[i], &player, &barre); // fonction qui permet de gérer les collisions
			done = quit_test(&bricks_band, &barre, &ball, &player); // fonction qui permet d'afficher un menu intermédiaire pour continuer/recommencer/arreter le jeu si le joueur à casser toutes les briques
			}
			temps_i = temps_f;
		}
	}
	clear(); // clear l'écran
	main_menu(); // appel du menu
	
}
