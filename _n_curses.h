#include <ncurses.h>
#ifndef _NCURSES_
#define _NCURSES_

#define NB_BRICK 21

typedef struct _WIN_struct {
  int score;
  int life;
  int level;
  int posX, posY;
  int height, width;
  WINDOW *win;
} WIN;

typedef struct _BALL_struct {
  WIN win;
  int dyx;
  int dyy;
  int start_dyx;
  int start_dyy;
  bool collided;
  int vitesse;
} BALL;

typedef struct _BRICK_struct{
  WIN win;
  bool collided;
  bool brick_break;
  int life;
  int color;
} BRICK;

typedef struct _BRICKS_BAND_struct{
  BRICK brick[NB_BRICK];
} BRICKS_BAND;

void propos (void);

void progress_bar (void);

void option(void);

void main_menu(void);

void init_brick(BRICK *brick,WIN *barre);

void init_bricks_band(BRICKS_BAND *bricks_band, WIN *barre);

bool quit_test(BRICKS_BAND *bricks_band, WIN *barre, BALL *ball, WIN *player);

void fill_brick(BRICK *brick);

void ncurses_initialiser();

void init_player(WIN *player, int height, int width, int lstarty, int lstartx);

void ncurses_stopper();

void ncurses_couleurs();

void reset_ball(BALL *ball);

void bounce_ball(BALL *ball, BRICK *brick, WIN *player, WIN *barre);

void inc_ball(BALL *ball);

void redraw_player(WIN *player);

void redraw_barre(WIN *barre);

void redraw_brick(BRICK *brick);

WINDOW *create_newwin_standard(int height, int width, int starty, int startx);

WINDOW *create_newwin_brick(BRICK *brick, int height, int width, int starty, int startx);

void destroy_win(WINDOW *local_win);

void init_ball(BALL *ball);

void historique (void);

int rand_sign();

int sign(const int x);

bool menu_choix(BRICKS_BAND *bricks_band, WIN *barre, BALL *ball, WIN *player);

#endif
