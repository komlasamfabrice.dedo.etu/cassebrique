CasseBrique
=

CasseBrique est un jeu de casse brique développé en C par des 5 étudiants de polytech lille spécialité [Informatique Microélectronique Automatique](https://www.polytech-lille.fr/formation/8-specialites/systemes-embarques/) dont le but est de détruire toutes les briques sans perdre la vie et de passer au niveau suivant .

Requirements
-

Le jeu de casseBrique se joue sur linux. Il nécessite :

* Un compilateur gcc ou clang
* Doxygen
* Librairie ncurses
* le paquet texlive-base

Cependant il est également possible de jouer au jeu de casseBrique sur un window's avec l'installation d'un virtual box.

Installation
-

Pour télécharger et jouer au jeu de casseBrique  vous pouver utiliser ces differentes commandes:

Installer la librairie ncurses si cette dernière n'existe pas déja.

  ```sh
sudo apt-get install libncurses5-dev libncursesw5-dev
  ```

Initialiser le dépot git pour télécharger le dossier du jeu

  ```sh
git init
  ```

  ```sh
git remote add OC  https://gitlab.univ-lille.fr/komlasamfabrice.dedo.etu/cassebrique.git
  ```

  ```sh
git clone  https://gitlab.univ-lille.fr/komlasamfabrice.dedo.etu/cassebrique.git
  ```

Une fois que le jeu est télécharger on peut faire un *make* pour construire l'excécutable

  ```sh
make
  ```

Pour lancer le jeu :

  ```sh
./casseBrique
  ```

Le jeu est composé d'un menu comme le montre la figure suivante:

![menu](../image/menu.png)

* La partie *Jouer* lance le jeu
* La partie *Historique* montre l'historique de toutes les personnes qui ont eu à jouer au jeu. On y voie le nom, le score et le niveau
* La partie *Option* montre les règles du jeu
* La partie *A propos* donne les informations sur les deloppeurs

Programmeurs
-
* Fabrice DEDO     <kdedo@polytech-lille.net>
* Lionel LELEU     <lionel.leleu@polytech-lille.net>
* Arthur LEPLA     <arthur.lepla@polytech-lille.net>
* Adrien DETRES    <adrien.detres@polytech-lille.net>
* Guillaume COFFRE <guillaume.coffre@polytech-lille.net>
