#include <stdlib.h>    /* Pour exit, EXIT_FAILURE */
#include <unistd.h>
#include "jeu.h" 
#include "_n_curses.h"
#include <string.h>
#include <time.h>
#include <stdbool.h>

#define NB_BRICK 21

/**
 * @brief Initialise la barre du joueur
 * 
 * @param player 
 * @param height 
 * @param width 
 * @param lstarty 
 * @param lstartx 
 */
void init_player(WIN *player, int height, int width, int lstarty, int lstartx)
{
  	WINDOW *player_win = create_newwin_standard(height, width, lstarty, lstartx);
  	player->win = player_win;
  	player->height = height;
  	player->width = width;
  	player->posX = lstartx;
  	player->posY = lstarty;
  	player->score = 0;
	player->life = 3;
	player->level = 0;
}

/**
 * @brief Créer les briques
 * @param brick 
 * @param barre 
 */
void init_brick(BRICK *brick, WIN *barre){

  int new_height, new_width;
  getmaxyx(stdscr,new_height,new_width);
  new_height = new_height - barre->height - 10;
  new_width = barre->width;

  brick->win.height = 5;
  brick->win.width = 20;
  brick->life = 3;
  brick->brick_break = false;
  brick->win.win=create_newwin_brick(brick,brick->win.height,brick->win.width,brick->win.posY,brick->win.posX);
}

/**
 * @brief Initialisation de toutes les briques
 * @param bricks_band
 * @param barre
 */
void init_bricks_band(BRICKS_BAND *bricks_band, WIN *barre){
	int x = 5, y = 7;	
	for(int i = 0; i < NB_BRICK; i++)
	{
	bricks_band->brick[i].win.posX = x;
	bricks_band->brick[i].win.posY = y;
	init_brick(&bricks_band->brick[i], barre);
	x = x + 30;
	if(x >= 200)
	{
	x = 5;
	y = y + 7;	
	}
	}
}

/**
 * @brief Fonction qui permet de quitter le jeu si plus brick ou plus de vie du joueur
 * @param bricks_band
 * @param barre
 * @param ball
 * @param player
 * @return bool
 */
bool quit_test(BRICKS_BAND *bricks_band, WIN *barre, BALL *ball, WIN *player){
	int compt = 0;
	
	for(int i = 0; i<NB_BRICK; i++)
	{
	if(bricks_band->brick[i].life <= 0)
	{
	compt++;
	}
	}
	if(player->life <= 0){
	return true;	
	}
	if(compt == NB_BRICK)
	{
	return menu_choix(bricks_band, barre, ball, player);
	}
	else{
	return false;	
	}
	
}

/**
 * @brief Initialisation de ncurses
 */
void ncurses_initialiser() {
  initscr();        /* Demarre le mode ncurses */
  nodelay(stdscr, true);
  cbreak();	        /* Pour les saisies clavier (desac. mise en buffer) */
  noecho();             /* Desactive l'affichage des caracteres saisis */
  keypad(stdscr, TRUE);	/* Active les touches specifiques */
  curs_set(FALSE);      /* Masque le curseur */
  refresh();            /* Met a jour l'affichage */
}

/**
 * @brief Fin de ncurses.
 */
void ncurses_stopper() {
  endwin();
}

/**
 * @brief Initialisation des couleurs.
 */
void ncurses_couleurs() {
  /* Verification du support de la couleur */
  if(has_colors() == FALSE) {
    ncurses_stopper();
    fprintf(stderr, "Le terminal ne supporte pas les couleurs.\n");
    exit(EXIT_FAILURE);
  }
}

/**
 * @brief Réinitialise la balle au milieu de l'écran
 * @param ball
 */
void reset_ball(BALL *ball) {
  ball->win.posX = COLS / 2 - 1;
  ball->win.posY = 22 + LINES / 2;
  ball->dyx = rand_sign();
  ball->dyy = -1;
}

/**
 * @brief Efface la fenêtre
 * @param local_win
 */
void destroy_win(WINDOW *local_win) {
  wborder(local_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
  wnoutrefresh(local_win);
  delwin(local_win);
}

/**
 * @brief Créer une nouvelle fenêtre standard et definit ses bordures
 * @param height
 * @param width
 * @param starty
 * @param startx
 * @return WINDOW*
 */
WINDOW *create_newwin_standard(const int height, const int width, const int starty, const int startx)
{
  WINDOW *local_win;
  local_win = newwin(height, width, starty, startx);
  box(local_win, 0, 0);
  wnoutrefresh(local_win);  /* Rafraichir la fenêtre	*/

  return local_win;
}

/**
 * @brief Créer une nouvelle fenêtre et definit ses bordures pour les briques
 * @param height
 * @param width
 * @param starty
 * @param startx
 * @return WINDOW*
 */
WINDOW *create_newwin_brick(BRICK *brick, const int height, const int width, const int starty, const int startx)
{
  WINDOW *local_win;

  local_win = newwin(height, width, starty, startx);
  int couleur = 1;
  init_pair(1,COLOR_RED,COLOR_BLACK); 
  init_pair(2,COLOR_YELLOW,COLOR_BLACK);
  init_pair(3,COLOR_GREEN,COLOR_BLACK);  

  if(brick->life == 1){
  brick->color = 1;
  }
  if(brick->life == 2){
  brick->color = 2;
  }
  if(brick->life >= 3){  
  brick->color = 3;
  }
  wattron(local_win,COLOR_PAIR(brick->color));
  box(local_win, 0, 0);
  wattroff(local_win,COLOR_PAIR(brick->color));
  wnoutrefresh(local_win);  /* Rafraichir la fenêtre	*/

  return local_win;
}

/**
 * @brief Permet de retourner 1 ou -1 avec 50% de chance
 * utiliser pour utiliser une direction aléatoire lors de l'initialisation
 * du jeu ou d'un reset
 * @return int
 */
int rand_sign() {
  return (float)rand() / ((float)INT32_MAX / 2.0f) <= 0.5 ? -1 : 1;
}

/**
 * @brief Utiliser pour rajouter de la vitesse a la balle
 * après une collision avec le plateau
 * @return int
 */
int sign(const int x) {
  if (x == 0) {
    return 0;
  }
  return x > 0 ? 1 : -1;
}


/**
 * @brief Permet a la balle de rebondir avec des direction aléatoire
 * @param ball
 * @param brick
 * @param player
 * @param barre
 */

void bounce_ball(BALL *ball, BRICK *brick, WIN *player, WIN *barre) {

  // Vérifie les collisions avec Player

  // gestion collision coin du joueur
  if((ball->win.posX + ball->win.width - 1 == player->posX && ball->win.posY + ball->win.height - 1 == player->posY) ||
  (ball->win.posX  == player->posX + player->width -1 && ball->win.posY + ball->win.height -1== player->posY)){
        ball->dyx = ball->dyx * -1;
        ball->dyy = ball->dyy * -1;
        ball->collided = true;
  }
  
  //gestion collision haut du joueur
  if (ball->win.posY + ball->win.height -1 == player->posY &&
      ball->win.posX + ball->win.width - 1 > player->posX &&
      ball->win.posX < player->posX + player->width - 1){
     ball->dyy = ball->dyy * -1; //change le signe (la balle rebondit mais dans le sens opposé)
    ball->collided = true;
  }

  //gestion collision gauche et droite du joueur
  if((ball->win.posY + ball->win.height - 1 > player->posY) && 
  (ball->win.posY < player->posY + player->height -1) && 
  (ball->win.posX + ball->win.width -1 == player->posX || ball->win.posX == player->posX + player->width -1)){
        ball->dyx = ball->dyx * -1;
        ball->collided = true;
  }
  
  // Vérifie la collision avec le haut de la fenêtre
  if (ball->win.posY <= barre->height-1) {
  ball->dyy = ball->dyy * -1; // change le signe (la balle rebondit mais dans le sens opposé)
  ball->collided = true;
  }

  //Reset quand la balle dépasse le bas du plateau
  if (ball->win.posY > LINES) {
    reset_ball(ball);
	player->life--;
  }

  // Vérifie la collision avec les extrémités gauche et droite de la fenêtre
  if (ball->win.posX + ball->win.width >= COLS || ball->win.posX <=0) {
    ball->dyx = ball->dyx * -1;
  }

  if(brick->brick_break == false)
  {
    // Vérifie la collision contre les briques

    //côté haut et basse de la brique sans les coins
    if((ball->win.posX + ball->win.width -1 > brick->win.posX) && 
    (ball->win.posX < brick->win.posX + brick->win.width -1) && 
    (ball->win.posY + ball->win.height -1 == brick->win.posY || ball->win.posY == brick->win.posY + brick->win.height -1)){
          ball->dyy = ball->dyy * -1;
          ball->collided = true;
          brick->life--;
    }
    //côté gauche et droite de la brique sans les coins
    if((ball->win.posY + ball->win.height -1 > brick->win.posY) && 
    (ball->win.posY < brick->win.posY + brick->win.height -1) && 
    (ball->win.posX + ball->win.width -1 == brick->win.posX || ball->win.posX == brick->win.posX + brick->win.width -1)){
          ball->dyx = ball->dyx * -1;
          ball->collided = true;
          brick->life--;
    }
    // coins de la brique
    if((ball->win.posX == brick->win.posX + brick->win.width -1 && ball->win.posY == brick->win.posY + brick->win.height -1) ||
    (ball->win.posX + ball->win.width -1 == brick->win.posX && ball->win.posY + ball->win.height - 1 == brick->win.posY) ||
    (ball->win.posX  == brick->win.posX + brick->win.width -1 && ball->win.posY + ball->win.height - 1 == brick->win.posY) ||
    (ball->win.posX + ball->win.width - 1 == brick->win.posX && ball->win.posY == brick->win.posY + brick->win.height -1)){
          ball->dyx = ball->dyx * -1;
          ball->dyy = ball->dyy * -1;
          ball->collided = true;
          brick->life--;
    }
  if(brick->life == 0 ){
  player->score++;
  }		
  redraw_brick(brick);
  }
  // redésine  le plateau du joueur après une collision
  
  redraw_player(player);
  redraw_barre(barre);	
  mvprintw(1,1,"Appuyer sur q pour quitter");
  mvprintw(3,1,"Appuyer sur r pour reset la position de la balle");
  mvprintw(1,COLS-70,"Nombre de vie du joueur: %d",player->life);
  mvprintw(1,COLS-30,"Briques cassees: %d",player->score);
  mvprintw(3,COLS-30,"Niveau du joueur: %d",player->level);

  

}


/**
 * @brief redessine le joueur
 * @param player
 * */
void redraw_player(WIN *player){
    destroy_win(player->win);
    player->win = create_newwin_standard(player->height, player->width, player->posY,
     player->posX);
}

/**
 * @brief redessine la barre
 * @param barre
 * */
void redraw_barre(WIN *barre){
    destroy_win(barre->win);
    barre->win = create_newwin_standard(barre->height, barre->width, barre->posY,
     barre->posX);
}

/**
 * @brief redessiner la brique
 * @param brick
 * */
void redraw_brick(BRICK *brick){
  destroy_win(brick->win.win);
  if(brick->life <= 0){
  brick->brick_break = true; 
  }
  else{
  brick->win.win = create_newwin_brick(brick, brick->win.height, brick->win.width, brick->win.posY,
  brick->win.posX);
  }
}

/**
 * @brief incrémente la position de la balle
 * @param ball
 * */
void inc_ball(BALL *ball){
    destroy_win(ball->win.win);
  	ball->win.posX += ball->dyx;
		ball->win.posY += ball->dyy;
		ball->win.win = create_newwin_standard(ball->win.height, ball->win.width,
		ball->win.posY, ball->win.posX);
}

/**
 * @brief Initialise la balle
 * @param ball
 */
void init_ball(BALL *ball)
{
  	ball->dyx = rand_sign();
  	ball->dyy = -1;
  	ball->collided = false;
	ball->vitesse = 70;
  	WIN ball_win;
  	ball_win.height = 2;
  	ball_win.width = 3;
  	ball_win.posX = COLS / 2 - ball_win.width / 2;
  	ball_win.posY = 22 + LINES / 2;
  	ball_win.win = create_newwin_standard(2, 3, ball_win.posY, ball_win.posX);
  	ball->win = ball_win;
}

/**
 * @brief Permet d'afficher un tableau avec l'historique
 */
void historique (void)
{
    int stop = 0;
    stop = 'a';
    char pseudo[30];
    int score=0;
    char date[30];
    int level =0;
    FILE *save = NULL ;

    save = fopen("historique.txt", "r+");
  
    //Init Ncurse
    ncurses_initialiser();
    int yMax = 0, xMax = 0, yMax2 = 0, xMax2 = 0; 
    int height = 0, width = 0, start_y = 0, start_x = 0;
    height = 30;
    width = 100;


    //Init Color
    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);


    //Get terminal size
    
    getmaxyx(stdscr, yMax, xMax);


    // Dimmension de la fenetre
    
    

    yMax -= height;
    xMax -= width;

    //Creation de la fenetre
    WINDOW *f_historique = NULL;
    f_historique = newwin(height, width, yMax/2, xMax/2);
    keypad(f_historique, true);// ACTIVE LE KEYPAD DANS LA FENETRE


    //Get fenetre size
    getmaxyx(stdscr, yMax2, xMax2);
    int posY = 0, posX = 0;
    posY = yMax2/2;
    posX =xMax2/2;
    int longueur = 0;
    longueur = strlen(" ##     ## ####  ######  ########  #######  ########  ####  #######  ##     ## ######## ");
    longueur = longueur/2;
    posX -= longueur;    
    getmaxyx(f_historique, yMax, xMax);
    xMax -= 12; // 12 cararctere dans casse-brique (pour mettre au millieu)

    //Affichage de la fenetre                                    
    attron(COLOR_PAIR(1));
    mvprintw(posY-24,posX," ##     ## ####  ######  ########  #######  ########  ####  #######  ##     ## ########  ");
    mvprintw(posY-23,posX," ##     ##  ##  ##    ##    ##    ##     ## ##     ##  ##  ##     ## ##     ## ##       ");
    mvprintw(posY-22,posX," ##     ##  ##  ##          ##    ##     ## ##     ##  ##  ##     ## ##     ## ##    ");
    mvprintw(posY-21,posX," #########  ##   ######     ##    ##     ## ########   ##  ##     ## ##     ## ######  ");
    mvprintw(posY-20,posX," ##     ##  ##        ##    ##    ##     ## ##   ##    ##  ##  ## ## ##     ## ##  ");
    mvprintw(posY-19,posX," ##     ##  ##  ##    ##    ##    ##     ## ##    ##   ##  ##    ##  ##     ## ##  " );
    mvprintw(posY-18,posX," ##     ## ####  ######     ##     #######  ##     ## ####  ##### ##  #######  ########");
    box(f_historique, 0, 0);
    wbkgd(f_historique,COLOR_PAIR(1));//couleur de la fenetre
    mvwprintw(f_historique, 1, xMax/2, "Casse-Brique");
    mvwprintw(f_historique, 3, xMax/8, "Nom");
    mvwprintw(f_historique, 3, 35, "Score");
    mvwprintw(f_historique, 3, 60, "Niveau");
    mvwprintw(f_historique, 3, 85, "Date ");
    

    for(int i = 3 ; i < yMax-1 ; i++)
    {
        mvwprintw(f_historique, i-1, 25, "|");
        mvwprintw(f_historique, i-1, 50, "|");
        mvwprintw(f_historique, i-1, 75, "|");
    }

    if (save != NULL) {
        //On lit dans le fichier
        int x = 5;
        int y = 5;
        while (fscanf(save,"%s   %d   %d   %s",pseudo,&score,&level,date)!=EOF){
            mvwprintw(f_historique,y,5,"%s",pseudo);
            mvwprintw(f_historique,y,36,"%d",score);
            mvwprintw(f_historique,y,62,"%d",level);
            mvwprintw(f_historique,y, 80, "%s",date);

            y++;

            wrefresh(f_historique);
        }
        fclose(save);
    }
    wrefresh(f_historique);
    while(1) {
        stop = getch();
        if(stop == 'q') {
            break;
        }
    }
	save = NULL;
    clear();
    endwin();
    main_menu();
}

/**
 * @brief Affiche les options
 */
void option(void)
{
	int stop =0;
	stop = 'a';
	
	//Init Ncurse
	ncurses_initialiser();

	//Init Color
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);


	//Get terminal size
 	int yMax = 0, xMax = 0, yMax2 = 0, xMax2 = 0;  
	getmaxyx(stdscr, yMax, xMax);


	// Dimmension de la fenetre
	int height = 0, width = 0, start_y = 0, start_x = 0;
	height = 15;
	width = 50;

	yMax -= height;
	xMax -= width;

	//Creation de la fenetre
	WINDOW *f_option = NULL;
	f_option = newwin(height, width, yMax/2, xMax/2);
	keypad(f_option, true);// ACTIVE LE KEYPAD DANS LA FENETRE


	//Get fenetre size
	getmaxyx(f_option, yMax, xMax);
    getmaxyx(stdscr, yMax2, xMax2);
    int posY = 0, posX = 0;
	posY = yMax2/2;
    posX =xMax2/2;
    int longueur = 0;
    longueur = strlen("  #######  ########  ######## ####  #######  ##    ##  ######  ");
    longueur = longueur/2;
    posX -= longueur;	
	xMax -= 12; // 12 cararctere dans casse-brique (pour mettre au millieu)

	//Affichage de la fenetre
 attron(COLOR_PAIR(1));
    mvprintw(posY-15,posX,"  #######  ########  ######## ####  #######  ##    ##  ######  ");
	mvprintw(posY-14,posX,"##     ## ##     ##    ##     ##  ##     ## ###   ## ##    ## ");
	mvprintw(posY-13,posX,"##     ## ##     ##    ##     ##  ##     ## ####  ## ##       ");
	mvprintw(posY-12,posX,"##     ## ########     ##     ##  ##     ## ## ## ##  ######  	");
	mvprintw(posY-11,posX,"##     ## ##           ##     ##  ##     ## ##  ####       ## ");
	mvprintw(posY-10,posX,"##     ## ##           ##     ##  ##     ## ##   ### ##    ## " );
	mvprintw(posY-9,posX, "#######  ##           ##    ####  #######  ##    ##  ######  ");
		

	attroff(COLOR_PAIR(1));


	box(f_option, 0, 0);
	wbkgd(f_option,COLOR_PAIR(1));//couleur de la fenetre
	mvwprintw(f_option, 1, 3+xMax/2, "Options");
	mvwprintw(f_option, 2, 1, "------------------------------------------------");
	mvwprintw(f_option, 3, 2+xMax/2, "Commandes");
	mvwprintw(f_option, 5, 2, "-> : Décaler le plateau a droite ");
	mvwprintw(f_option, 6, 2, "<- : Décaler le plateau a gauche ");
	mvwprintw(f_option, 7, 2, " q : Pour quitter  ");
	mvwprintw(f_option, 8, 2, " r : Pour reset ");
	mvwprintw(f_option, 9, 2, " s : Pour sauvegarder ");
	wrefresh(f_option);
	stop = getch();
	while(1)
	{
		stop = getch();
		if(stop == 'q')
		{
			break;
		}
	}
	clear();
	endwin();
	main_menu();

}

/**
 * @brief  Donne les informations sur les développeurs
 *
 */
void propos (void)
{
    int stop_propos = 0;
    
    //Init Ncurse
    ncurses_initialiser();

    //Init Color
    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);


    //Get terminal size
    int yMax, xMax, yMax2, xMax2;  
    getmaxyx(stdscr, yMax, xMax);


    // Dimmension de la fenetre
    int height, width, start_y, start_x;
    height = 14;
    width = 65;

    yMax -= height;
    xMax -= width;

    //Creation de la fenetre
    WINDOW *f_propos = newwin(height, width, yMax/2, xMax/2);
    keypad(f_propos, true);// ACTIVE LE KEYPAD DANS LA FENETRE


    //Get fenetre size
    getmaxyx(f_propos, yMax, xMax);
    getmaxyx(stdscr, yMax2, xMax2);
    int posY =yMax2/2;
    int posX =xMax2/2;
    int longueur;
    longueur = strlen("    ###          ########  ########   #######  ########   #######   ######  ");
    longueur = longueur/2;
    posX -= longueur;	
    xMax -= 8; // 12 cararctere dans casse-brique (pour mettre au millieu)

    //Affichage de la fenetre
    attron(COLOR_PAIR(1));
    mvprintw(posY-15,posX,"    ###          ########  ########   #######  ########   #######   ######  ");
	mvprintw(posY-14,posX,"   ## ##         ##     ## ##     ## ##     ## ##     ## ##     ## ##    ## ");
	mvprintw(posY-13,posX,"  ##   ##        ##     ## ##     ## ##     ## ##     ## ##     ## ##       ");
	mvprintw(posY-12,posX," ##     ##       ########  ########  ##     ## ########  ##     ##  ######  ");
	mvprintw(posY-11,posX," #########       ##        ##   ##   ##     ## ##        ##     ##       ## ");
	mvprintw(posY-10,posX," ##     ##       ##        ##    ##  ##     ## ##        ##     ## ##    ## " );
	mvprintw(posY-9,posX, " ##     ##       ##        ##     ##  #######  ##         #######   ######  ");
		

	attroff(COLOR_PAIR(1));


    box(f_propos, 0, 0);
    wbkgd(f_propos,COLOR_PAIR(1));//couleur de la fenetre
    mvwprintw(f_propos, 1, xMax/2, "A propos");
    mvwprintw(f_propos, 2, 1, "---------------------------------------------------------------");
    mvwprintw(f_propos, 3, 2, "Salut jeune joueur, merci pour ta curiosité !");
    mvwprintw(f_propos, 4, 2, "Merci d'avoir télécharger notre jeu.");
    mvwprintw(f_propos, 5, 2, "Ce projet à été réaliser à Polytech-Lille.");
    mvwprintw(f_propos, 6, 2, "Composer de DEDO Fabrice, DETRES Adrien,");
    mvwprintw(f_propos, 7, 2, "COFFRE Guillaume, LELEU Lionel, LEPLA Arthur");
    mvwprintw(f_propos, 9, 2, "GitHub :https://gitlab.univ-lille.fr/q%22");
    mvwprintw(f_propos, 10, 2, "komlasamfabrice.dedo.etu/cassebrique.git ");
    mvwprintw(f_propos, 12, 2, "@copyright all right reserved ... ");
    wrefresh(f_propos);
    stop_propos = getch();

    while(1)
	{
		stop_propos= getch();
		if(stop_propos == 'q')
		{
			break;
		}
	}
	clear();
	endwin();
	main_menu();
}

/**
 * @brief  Permet de générer le menu principal
 */
void main_menu(void)
{
	//Init Ncurse
	ncurses_initialiser();
	//Init Color
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);


	//Get terminal size
	int yMax = 0, xMax = 0, yMax2 = 0, xMax2 = 0;
	getmaxyx(stdscr, yMax, xMax);


	// Dimmension de la fenetre
	int height = 0, width = 0, start_y = 0, start_x = 0;
	height = 11;
	width = 20;

	yMax -= height;
	xMax -= width;

	//Creation de la fenetre
	WINDOW *fenetre =  NULL;
	fenetre = newwin(height, width, yMax/2, xMax/2);
	keypad(fenetre, true);// ACTIVE LE KEYPAD DANS LA FENETRE

	char list[5][12] = {"Jouer     ", "Historique", "Options   ","A propos  ","Quitter    "};
	char item [12] = {'J','o','u','e','r'} ;
	int choice = 0;
	int i =0;

	//Get fenetre size

    getmaxyx(stdscr, yMax2, xMax2);
    int posY = 0, posX = 0;
	posY = yMax2/2;
    posX =xMax2/2;
    int longueur = 0;
    longueur = strlen(" ##     ## ######## ##    ## ##     ## ");
    longueur = longueur/2;
    posX -= longueur;	
	getmaxyx(fenetre, yMax, xMax);
	xMax -= 12; // 12 cararctere dans casse-brique (pour mettre au millieu)

	//Affichage de la fenetre



	box(fenetre, 0, 0);
	wbkgd(fenetre,COLOR_PAIR(1));//couleur de la fenetre
	mvwprintw(fenetre, 1, xMax/2, "Casse-Brique");
	wrefresh(fenetre);

	for(i = 0; i<5; i++ )
	{
		if( i == 0 )
		{
			wattron( fenetre, A_STANDOUT ); // highlights the first item.
			mvwprintw( fenetre, i+3, 2, "%s", item );
		}

		else
		{
			wattroff( fenetre, A_STANDOUT );
			sprintf(item, "%-11s",  list[i]);
			mvwprintw( fenetre, i+3, 2, "%s", item );
			
		}
	}
	wrefresh(fenetre); // update the terminal screen
	i = 0;
	
	while( choice!= 'q')
	{
		choice = wgetch(fenetre);
		// right pad with spaces to make the items appear with even width.
		sprintf(item, "%-11s",  list[i]);
		mvwprintw( fenetre, i+3, 2, "%s", item );
		// use a variable to increment or decrement the value based on the input.
		switch( choice )
			{
				case KEY_UP:
					i--;
					i = ( i<0 ) ? 4 : i;
					break;
				case KEY_DOWN:
					i++;
					i = ( i>4 ) ? 0 : i;
					break;
				case 10:
					mvwprintw(fenetre,yMax-2,2,"%s",list[i]);
					mvwprintw(fenetre,yMax-2,14,"%d",i);
					clear();
					if(i==0)
					{
						//jeu
						//wclear(fenetre);
						//wrefresh(fenetre);
						choice ='q';
						f_jeu();
					}
					if(i==1)
					{
						//Historique
						//wclear(fenetre);
						//wrefresh(fenetre);
						choice ='q';
						historique();

					}
					if(i==2)
					{
						//options
						//wclear(fenetre);
						//wrefresh(fenetre);
						choice ='q';
						option();
					}
					if(i==3)
					{
						// a propos
						//wclear(fenetre);
						//wrefresh(fenetre);
						choice ='q';
						propos();
					}
					if(i==4)
					{
						//wclear(fenetre);
						//wrefresh(fenetre);
						choice ='q';
					}
					break;

			}
// now highlight the next item in the list.
		wattron( fenetre, A_STANDOUT );
		sprintf(item, "%-11s",  list[i]);
		mvwprintw( fenetre, i+3, 2, "%s", item);
		wattroff( fenetre, A_STANDOUT );
		}
	//desallouer la mémoire
	//terminer ncurses
	endwin();
}

/**
 * @brief  Permet de générer la barre de progression
 */
void progress_bar (void)
{
    int stop_bar = 0;
	    initscr();
    cbreak();
    noecho();//n affiche pas les carracterers tapés au clavier

    //Init Color
    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);


    //Get terminal size
    int yMax = 0, xMax = 0, yMax2 = 0, xMax2 = 0;    
    getmaxyx(stdscr, yMax, xMax);

    // Dimmension de la fenetreq
    int height = 0, width = 0, start_y = 0, start_x = 0;
    height = 3;
    width = 100;

    yMax -= height;
    xMax -= width;

    //Creation de la fenetre
    WINDOW *test = NULL;
	test = newwin(height, width, yMax/2, xMax/2);
    refresh();
    wrefresh(test);
    keypad(test, true);// ACTIVE LE KEYPAD DANS LA FENETRE


    //Get fenetre size
    getmaxyx(test, yMax, xMax);
    getmaxyx(stdscr, yMax2, xMax2);


    //xMax -= 10; // 12 cararctere dans casse-brique (pour mettre au millieu)

    //Affichage de la fenetre
    int posY = 0, posX = 0;
	posY = yMax2/2;
    posX =xMax2/2;
    int longueur = 0;
	longueur = strlen("_____       ___   _____   _____   _____        _____   _____    _   _____     _   _   _____");
    longueur = longueur/2;
    posX -= longueur;

    int longueur2 = strlen("  _____   _____   _      __    __  _____   _____   _____   _   _        _       _   _       _       _____  ");
    longueur2 = longueur2/2;
     int posX2 =xMax2/2;
    posX2 -= longueur;

    box(test, 0, 0);
    wbkgd(test,COLOR_PAIR(1));//couleur de la fenetre
    attron(COLOR_PAIR(1));
    mvprintw(posY-10,posX," _____       ___   _____   _____   _____        _____   _____    _   _____     _   _   _____");
	mvprintw(posY-9,posX,"|  ___|     /   | |  ___| |  ___| | ____|      |  _  | |  _  |  | | |  _  |   | | | | | ____|");
	mvprintw(posY-8,posX,"| |        / /| | | |___  | |___  | |__        | |_| | | |_| |  | | | | | |   | | | | | |__");
	mvprintw(posY-7,posX,"| |       / /_| | |___  | |___  | |  __|       |  _  { |  _  /  | | | | | |   | | | | |  __|");
	mvprintw(posY-6,posX,"| |___   / /__| |  ___| |  ___| | | |___       | |_| | | | | |  | | | |_| |_  | |_| | | |___");
	mvprintw(posY-5,posX,"|_____| /_/   |_| |_____| |_____| |_____|      |_____| |_|  |_| |_| |_______| |_____| |_____|");

	mvprintw(posY+5,posX2-6, " _____   _____   _      __    __  _____   _____   _____   _   _        _       _   _       _       _____  ");
	mvprintw(posY+6,posX2-6, "|  _  l /  _  l | |     l l  / / |_   _| | ____| /  ___| | | | |      | |     | | | |     | |     | ____| ");
	mvprintw(posY+7,posX2-6, "| |_| | | | | | | |      l l/ /    | |   | |__   | |     | |_| |      | |     | | | |     | |     | |__   ");
	mvprintw(posY+8,posX2-6, "|  ___/ | | | | | |       l  /     | |   |  __|  | |     |  _  |      | |     | | | |     | |     |  __|");
	mvprintw(posY+9,posX2-6, "| |     | |_| | | |___    l /      | |   | |___  | |___  | | | |      | |___  | | | |___  | |___  | |___");
	mvprintw(posY+10,posX2-6,"|_|     l_____/ |_____|  /_/       |_|   |_____| l_____| |_| |_|      |_____| |_| |_____| |_____| |_____| ");


	attroff(COLOR_PAIR(1));
	int pos = 0;
    while(stop_bar != 10)
    {
    for (int i = 1;i < 99;i++)
    {
        wattron(test,A_REVERSE);
        mvwaddch(test,1,i,' ');
        wattroff(test,A_REVERSE);
        wrefresh(test);
        usleep(5000);
    }
    	pos =xMax/2;
    	pos -= 6;
    	mvwprintw(test ,1 ,pos," PRESS ENTER");

    	wrefresh(test);
    	stop_bar = getch();

        if (stop_bar == 10)
            {
                wclear(test);
                wrefresh(test);
                clear();
                refresh();
            }
    }
	endwin();
}

/**
 * @brief Fonction qui permet d'afficher un menu lorsqu'il n'y a plus de brique
 * @param bricks_band
 * @param barre
 * @param ball
 * @param player
 * */
bool menu_choix(BRICKS_BAND *bricks_band, WIN *barre, BALL *ball, WIN *player)
{
	//Init Color
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);

	//Get terminal size
	int yMax = 0, xMax = 0, yMax2 = 0, xMax2 = 0;
	getmaxyx(stdscr, yMax, xMax);

	bool done = false;

	// Dimmension de la fenetre
	int height = 0, width = 0, start_y = 0, start_x = 0;
	height = 8;
	width = 30	;

	yMax -= height;
	xMax -= width;

	//Creation de la fenetre
	WINDOW *fenetre =  NULL;
	fenetre = newwin(height, width, yMax/2, xMax/2);
	keypad(fenetre, true);// ACTIVE LE KEYPAD DANS LA FENETRE

	char list[5][12] = {"Niv suivant", "Recommencer","Quitter     "};
	char item [12] = {'N','i','v',' ','s','u','i','v','a','n','t'} ;
	int choice = 0;
	int i =0;

	//Get fenetre size

    getmaxyx(stdscr, yMax2, xMax2);
    int posY = 0, posX = 0;
	posY = yMax2/2;
    posX =xMax2/2;
    int longueur = 0;
    posX -= longueur;	
	getmaxyx(fenetre, yMax, xMax);
	xMax -= 12; // 12 cararctere dans casse-brique (pour mettre au millieu)

	//Affichage de la fenetre



	box(fenetre, 0, 0);
	wbkgd(fenetre,COLOR_PAIR(1));//couleur de la fenetre
	mvwprintw(fenetre, 1, 2, "Que veux-tu faire ?");
	wrefresh(fenetre);

	for(i = 0; i<3; i++ )
	{
		if( i == 0 )
		{
			wattron( fenetre, A_STANDOUT ); // highlights the first item.
			mvwprintw( fenetre, i+3, 2, "%s", item );
		}

		else
		{
			wattroff( fenetre, A_STANDOUT );
			sprintf(item, "%-11s",  list[i]);
			mvwprintw( fenetre, i+3, 2, "%s", item );
			
		}
	}
	wrefresh(fenetre); // update the terminal screen
	i = 0;

	while( choice!= 'q')
	{
		choice = wgetch(fenetre);
		// right pad with spaces to make the items appear with even width.
		sprintf(item, "%-11s",  list[i]);
		mvwprintw( fenetre, i+3, 2, "%s", item );
		// use a variable to increment or decrement the value based on the input.
		switch( choice )
			{
				case KEY_UP:
					i--;
					i = ( i<0 ) ? 2 : i;
					break;
				case KEY_DOWN:
					i++;
					i = ( i>2 ) ? 0 : i;
					break;
				case 10:
					mvwprintw(fenetre,yMax-2,2,"%s",list[i]);
					mvwprintw(fenetre,yMax-2,14,"%d",i);
					clear();
					if(i==0)
					{
						//continuer fonction
						choice='q';	
						wclear(fenetre);
						wrefresh(fenetre);
						for(int i = 0; i<NB_BRICK; i++)
							{
							ball->vitesse=ball->vitesse-5;
							init_bricks_band(bricks_band, barre);
							bricks_band->brick[i].brick_break = false;
							player->level++;	
							}
					}
					if(i==1)
					{
						//recommencer  fonction
						choice='q';	
						wclear(fenetre);
						wrefresh(fenetre);
						for(int i = 0; i<NB_BRICK; i++)
							{
							init_bricks_band(bricks_band, barre);
							bricks_band->brick[i].brick_break = false;
							player->score = 0;
							player->level = 0;	
							}
					}
					if(i==2)
					{
						//quitter fonction
						wclear(fenetre);
						wrefresh(fenetre);
						choice='q';
						done = true;
						//main_menu();
					}

					break;

			}
// now highlight the next item in the list.
		wattron( fenetre, A_STANDOUT );
		sprintf(item, "%-11s",  list[i]);
		mvwprintw( fenetre, i+3, 2, "%s", item);
		wattroff( fenetre, A_STANDOUT );
		}
	//desallouer la mémoire
	//terminer ncurses
	endwin();
	return done;
}