#include <ncurses.h>   /* Pour toutes les fonctions/constantes ncurses */
#include <stdlib.h>    /* Pour exit, EXIT_FAILURE */
#include <unistd.h>
#include <string.h>

void progress_circle(char *nom,int *taille) {
	int leave = 0;
	while(leave != 10){
		initscr();//initialisation de l'écran pour ncurse
		cbreak();
		noecho();//n affiche pas les carracterers tapés au clavier

		//Init Color
		start_color();
		init_pair(1, COLOR_GREEN, COLOR_BLACK);

		//Get terminal size
		int yMax, xMax;
		getmaxyx(stdscr, yMax, xMax);

		// Dimmension de la fenetre
		int height, width, start_y, start_x;
		height = 3;
		width = 25;

		int ymaxf,xmaxf;
		ymaxf = yMax/2;
		xmaxf = xMax/2;

		yMax -= height;
		xMax -= width;

		//Creation de la fenetre
		WINDOW *test = newwin(height, width, yMax/2, xMax/2);
		refresh();
		int a=0, b=0;
		wrefresh(test);
		keypad(test, true);// ACTIVE LE KEYPAD DANS LA FENETRE

		//Affichage de la fenetre
		box(test, 0, 0);
		//getmaxyx(test, wy, wx);
		wbkgd(test,COLOR_PAIR(1));//couleur de la fenetre

		mvprintw(ymaxf-(height/2)-1,xmaxf-(width/2)+2,"Entrer votre pseudo");
		mvprintw(ymaxf-(height/2)+3,xmaxf-(width)+2,"Votre pseudo ne peut pas faire plus de 23 cararcteres");
		wrefresh(test);
		int c = 0;//var. qui reçoit les cararctere saisis
		int xpointeur = 0;//var. qui va reçoit les coordonnées X dans la box
		int cpt = 0;//var. qui reçoit le nbre de car dans la box
		move(ymaxf,xmaxf-(width/2));
		wrefresh(test);
		int n = 0;//var. du nbre de cararctere dans mon nom
		//char nom[n];
		while (cpt < 23) {
			//echo();
			c=getch();
			switch (c) {
				case 127:
					mvprintw(0,0,"Enter case 127 while \n");
					nocbreak();
					xpointeur = xpointeur + 2;
					mvwdelch(test,1,xpointeur);
					xpointeur--;
					mvwdelch(test,1,xpointeur);
					xpointeur--;
					mvwdelch(test,1,xpointeur);
					xpointeur--;
					n = n - 1;
					taille = &n;
					cpt = cpt-1;
					break;
				case 10:
				 noecho();
					attron(A_BLINK);
					mvprintw(ymaxf-(height/2)+5,xmaxf-(width/2)+5,"Press Enter");
					attroff(A_BLINK);
					leave = getch();
					if (leave == 10) {
						clear();
						refresh();
						break;
					}
					break;
				default:
					echo();
					nocbreak();
					char m = 0;
					while (m != 10 && m != EOF && cpt <23) {
						m = getch();
						nom[n] = m;
						n++;
						taille = &n;
					}
					xpointeur++;
					cpt++;
					//endwin();
					refresh();
					break;

			}
			break;
		}
		nocbreak();
		noecho();
	}
	endwin();
}

int main(void) {
	char *nom;
	int taille = 0;
	// initscr();//initialisation de l'écran pour ncurse
	// cbreak();
	// noecho();//n affiche pas les carracterers tapés au clavier
	//
	//
	// int test = 0;
	// test = getch();
	//
	// mvprintw(0,0,"%d",test);
	//
	// getch();
	// endwin();

	progress_circle(nom, &taille);
	mvprintw(0,0,"%s",nom);
	for(int i=0;i<taille;i++){
		mvprintw(0,i,"%s",nom[i]);
	}
	mvprintw(1,0,"\n");
	refresh();
	while(1)
	return(0);
}
